import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

export default function Square() {
    const [value, setValue] = useState(null);
    function handleCLickSquareButton() {
      setValue("X");
    }
  
    return (
      <button className="square" onClick={handleCLickSquareButton}>
        {value}
      </button>
    );
}

const styles = StyleSheet.create({})
