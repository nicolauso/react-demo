//Les valeurs du nom, de l'image de profil et du statut connecté (représenté par le rond vert, ou rouge) devront provenir du composant parent App.jsx.

export default function Contact({ person, imgLink, status }) {

    const styleDotTrue = {
        width: '10px',
        height: '10px',
        borderRadius: '50%', 
        backgroundColor: 'green',  
      };

      const styleDotFalse = {
        width: '10px',
        height: '10px',
        borderRadius: '50%', 
        backgroundColor: 'red',  
      };


  return (
    <>
      
      <div>
          <img
            className="avatar"
            src={imgLink}
            alt="Lin Lanying"
            width={100}
            height={100}
            style={{ borderRadius: "50%" }}
          />
          <div className="status-dot" style={status ? styleDotTrue : styleDotFalse}>{status}</div>
          <h1>
            {person.lastname} {person.firstname}
          </h1>
      </div>
    </>
  );
}
